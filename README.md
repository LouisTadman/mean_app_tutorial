# README #

This is me following along with the How to build a M.E.A.N web application tutorial https://www.youtube.com/watch?v=OhPFgqHz68o

This repo includes all node modules and all node specific files needed to start the server. You will need mongodb installed and a data directory configured.

### Notes ###
* [Commit](https://bitbucket.org/LouisTadman/mean_app_tutorial/commits/106a2c6c4c855665d1e848aa29dd84926058d557?at=master) was made around the [36:18](https://youtu.be/OhPFgqHz68o?t=36m12s) min mark and was a successful build.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact